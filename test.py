import pyfib
import time
from typing import Callable

def fib_iter(n: int):
    if n < 2:
        return n
    pre_last = 0
    last = 1
    cur = 0
    for i in range(2, n + 1):
        cur = last + pre_last
        pre_last = last
        last = cur
    return cur

def fib_rec(n: int):
    if n < 2:
        return n
    return fib_rec(n - 1) + fib_rec(n - 2)

def time_func(func: Callable, arg: int):
    start = time.time()
    res = func(arg)
    stop = time.time()
    print(f'Function {func.__name__}({arg}) took {stop - start} seconds. Result: {res}')

time_func(fib_iter, 93)
time_func(pyfib.c_fib_iter, 93)
time_func(fib_rec, 37)
time_func(pyfib.c_fib_rec, 37)
print(pyfib.version())
